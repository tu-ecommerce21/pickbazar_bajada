/* c4043e148a489584124ea4f03d2106ff3bdcbbf8
 * This file is automatically generated by graphql-let. */

import * as Types from "graphql-let/__generated__/__types__";
import * as Apollo from '@apollo/client';
export declare type ShippingClassesQueryVariables = Types.Exact<{
  text?: Types.InputMaybe<Types.Scalars['String']>;
  orderBy?: Types.InputMaybe<Array<Types.QueryShippingClassesOrderByOrderByClause> | Types.QueryShippingClassesOrderByOrderByClause>;
}>;
export declare type ShippingClassesQuery = {
  __typename?: 'Query';
  shippingClasses: Array<{
    __typename?: 'Shipping';
    id: string;
    name: string;
    amount: number;
    is_global: boolean;
    type: Types.ShippingType;
  }>;
};
export declare type ShippingClassQueryVariables = Types.Exact<{
  id: Types.Scalars['ID'];
}>;
export declare type ShippingClassQuery = {
  __typename?: 'Query';
  shippingClass: {
    __typename?: 'Shipping';
    id: string;
    name: string;
    amount: number;
    is_global: boolean;
    type: Types.ShippingType;
  };
};
export declare type CreateShippingClassMutationVariables = Types.Exact<{
  input: Types.CreateShippingInput;
}>;
export declare type CreateShippingClassMutation = {
  __typename?: 'Mutation';
  createShipping: {
    __typename?: 'Shipping';
    id: string;
    name: string;
    amount: number;
    is_global: boolean;
    type: Types.ShippingType;
  };
};
export declare type UpdateShippingClassMutationVariables = Types.Exact<{
  input: Types.UpdateShippingInput;
}>;
export declare type UpdateShippingClassMutation = {
  __typename?: 'Mutation';
  updateShipping: {
    __typename?: 'Shipping';
    id: string;
    name: string;
    amount: number;
    is_global: boolean;
    type: Types.ShippingType;
  };
};
export declare type DeleteShippingClassMutationVariables = Types.Exact<{
  id: Types.Scalars['ID'];
}>;
export declare type DeleteShippingClassMutation = {
  __typename?: 'Mutation';
  deleteShipping: {
    __typename?: 'Shipping';
    id: string;
    name: string;
    amount: number;
    is_global: boolean;
    type: Types.ShippingType;
  };
};
export declare const ShippingClassesDocument: Apollo.DocumentNode;
/**
 * __useShippingClassesQuery__
 *
 * To run a query within a React component, call `useShippingClassesQuery` and pass it any options that fit your needs.
 * When your component renders, `useShippingClassesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useShippingClassesQuery({
 *   variables: {
 *      text: // value for 'text'
 *      orderBy: // value for 'orderBy'
 *   },
 * });
 */

export declare function useShippingClassesQuery(baseOptions?: Apollo.QueryHookOptions<ShippingClassesQuery, ShippingClassesQueryVariables>): Apollo.QueryResult<ShippingClassesQuery, Types.Exact<{
  text?: Types.InputMaybe<string> | undefined;
  orderBy?: Types.InputMaybe<Types.QueryShippingClassesOrderByOrderByClause | Types.QueryShippingClassesOrderByOrderByClause[]> | undefined;
}>>;
export declare function useShippingClassesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ShippingClassesQuery, ShippingClassesQueryVariables>): Apollo.LazyQueryResultTuple<ShippingClassesQuery, Types.Exact<{
  text?: Types.InputMaybe<string> | undefined;
  orderBy?: Types.InputMaybe<Types.QueryShippingClassesOrderByOrderByClause | Types.QueryShippingClassesOrderByOrderByClause[]> | undefined;
}>>;
export declare type ShippingClassesQueryHookResult = ReturnType<typeof useShippingClassesQuery>;
export declare type ShippingClassesLazyQueryHookResult = ReturnType<typeof useShippingClassesLazyQuery>;
export declare type ShippingClassesQueryResult = Apollo.QueryResult<ShippingClassesQuery, ShippingClassesQueryVariables>;
export declare const ShippingClassDocument: Apollo.DocumentNode;
/**
 * __useShippingClassQuery__
 *
 * To run a query within a React component, call `useShippingClassQuery` and pass it any options that fit your needs.
 * When your component renders, `useShippingClassQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useShippingClassQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */

export declare function useShippingClassQuery(baseOptions: Apollo.QueryHookOptions<ShippingClassQuery, ShippingClassQueryVariables>): Apollo.QueryResult<ShippingClassQuery, Types.Exact<{
  id: string;
}>>;
export declare function useShippingClassLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ShippingClassQuery, ShippingClassQueryVariables>): Apollo.LazyQueryResultTuple<ShippingClassQuery, Types.Exact<{
  id: string;
}>>;
export declare type ShippingClassQueryHookResult = ReturnType<typeof useShippingClassQuery>;
export declare type ShippingClassLazyQueryHookResult = ReturnType<typeof useShippingClassLazyQuery>;
export declare type ShippingClassQueryResult = Apollo.QueryResult<ShippingClassQuery, ShippingClassQueryVariables>;
export declare const CreateShippingClassDocument: Apollo.DocumentNode;
export declare type CreateShippingClassMutationFn = Apollo.MutationFunction<CreateShippingClassMutation, CreateShippingClassMutationVariables>;
/**
 * __useCreateShippingClassMutation__
 *
 * To run a mutation, you first call `useCreateShippingClassMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateShippingClassMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createShippingClassMutation, { data, loading, error }] = useCreateShippingClassMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */

export declare function useCreateShippingClassMutation(baseOptions?: Apollo.MutationHookOptions<CreateShippingClassMutation, CreateShippingClassMutationVariables>): Apollo.MutationTuple<CreateShippingClassMutation, Types.Exact<{
  input: Types.CreateShippingInput;
}>, Apollo.DefaultContext, Apollo.ApolloCache<any>>;
export declare type CreateShippingClassMutationHookResult = ReturnType<typeof useCreateShippingClassMutation>;
export declare type CreateShippingClassMutationResult = Apollo.MutationResult<CreateShippingClassMutation>;
export declare type CreateShippingClassMutationOptions = Apollo.BaseMutationOptions<CreateShippingClassMutation, CreateShippingClassMutationVariables>;
export declare const UpdateShippingClassDocument: Apollo.DocumentNode;
export declare type UpdateShippingClassMutationFn = Apollo.MutationFunction<UpdateShippingClassMutation, UpdateShippingClassMutationVariables>;
/**
 * __useUpdateShippingClassMutation__
 *
 * To run a mutation, you first call `useUpdateShippingClassMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateShippingClassMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateShippingClassMutation, { data, loading, error }] = useUpdateShippingClassMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */

export declare function useUpdateShippingClassMutation(baseOptions?: Apollo.MutationHookOptions<UpdateShippingClassMutation, UpdateShippingClassMutationVariables>): Apollo.MutationTuple<UpdateShippingClassMutation, Types.Exact<{
  input: Types.UpdateShippingInput;
}>, Apollo.DefaultContext, Apollo.ApolloCache<any>>;
export declare type UpdateShippingClassMutationHookResult = ReturnType<typeof useUpdateShippingClassMutation>;
export declare type UpdateShippingClassMutationResult = Apollo.MutationResult<UpdateShippingClassMutation>;
export declare type UpdateShippingClassMutationOptions = Apollo.BaseMutationOptions<UpdateShippingClassMutation, UpdateShippingClassMutationVariables>;
export declare const DeleteShippingClassDocument: Apollo.DocumentNode;
export declare type DeleteShippingClassMutationFn = Apollo.MutationFunction<DeleteShippingClassMutation, DeleteShippingClassMutationVariables>;
/**
 * __useDeleteShippingClassMutation__
 *
 * To run a mutation, you first call `useDeleteShippingClassMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteShippingClassMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteShippingClassMutation, { data, loading, error }] = useDeleteShippingClassMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */

export declare function useDeleteShippingClassMutation(baseOptions?: Apollo.MutationHookOptions<DeleteShippingClassMutation, DeleteShippingClassMutationVariables>): Apollo.MutationTuple<DeleteShippingClassMutation, Types.Exact<{
  id: string;
}>, Apollo.DefaultContext, Apollo.ApolloCache<any>>;
export declare type DeleteShippingClassMutationHookResult = ReturnType<typeof useDeleteShippingClassMutation>;
export declare type DeleteShippingClassMutationResult = Apollo.MutationResult<DeleteShippingClassMutation>;
export declare type DeleteShippingClassMutationOptions = Apollo.BaseMutationOptions<DeleteShippingClassMutation, DeleteShippingClassMutationVariables>;